﻿namespace UISystem
{
    public enum PopupName
    {
        None = 0,
        ConfirmationPopup
    }
    public enum ScreenName
    {
        None = 0,
        WelcomeScreen,
        HomeMenuScreen,
        WorldSelectionScreen,
        PlaneDetectionScreen,
        GamePlayScreen,
        FreestyleScreen,
        SelfieModeScreen,
        GameOverScreen,
        LoadingScreen,
        SaveWorldSelfieScreen
    }
}